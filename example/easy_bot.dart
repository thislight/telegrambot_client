import 'package:telegrambot_client/bot.dart';
import 'package:telegrambot_client/router.dart' show CMDLikeRouter;
import 'apitoken.dart' show TOKEN; // Put your token in this file

final TelegramBot bot = new TelegramBot(TOKEN);

void main(){
    CMDLikeRouter router = new CMDLikeRouter({
        "start": (Message m,_){
            bot.reply(m, "Hello, try to use /1s");
        },
        "1s": (Message m,_){
            bot.reply(m, "Wow, you +1s to Zemin Jiang!");
        }
    });
    bot.addRouter(router);
    bot.start();
}

