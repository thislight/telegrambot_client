import 'bot.dart' show TelegramBot;


abstract class Plugin{
    void init(TelegramBot bot);
}