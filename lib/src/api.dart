import 'requests.dart' as reqs;
import 'dart:async' show Future,Completer,Stream,StreamController,Timer;
import 'dart:convert' show JSON,UTF8;
import 'keybroad.dart' show KeybroadMarkup;
import 'package:cli_util/cli_logging.dart' show Logger;


final Logger logger = new Logger.verbose();

const MIME_JSON = "application/json";


const MIME_FILE = "multipart/form-data";


typedef UpdatesPollingTimerCallback(Timer timer);


class APIRequest{
    String method;
    Map<String,dynamic> data;

    APIRequest(this.method,[this.data=const {}]);
}


class APIBack{
    APIRequest _request;
    dynamic _data;

    APIBack(this._request,this._data);

    dynamic operator[](dynamic key) => data[key];

    dynamic get data => _data;

    APIRequest get request => _request;

    bool get ok => this["ok"];
    String get description => this["description"];

    String toString() => JSON.encode(data);
}


void processContentTypeAsJson(reqs.Request req){
    req.headers.set("Content-Type", MIME_JSON);
}


class User{
    int _id;
    String _firstName;
    String _lastName;
    String _username;
    String _lang;

    int get id => _id;
    String get firstName => _firstName;
    String get lastName => _lastName;
    String get username => _username;
    String get lang => _lang;

    User(this._id,this._firstName,[this._lastName,this._username,this._lang]);

    factory User.fromMap(Map<String, dynamic> map){
        return new User(
            map["id"],
            map["first_name"],
            map["last_name"],
            map["username"],
            map["language_code"]
        );
    }
}


class ChatPhoto{
    String _smallFileId;
    String _bigFileId;

    ChatPhoto(this._smallFileId,this._bigFileId);

    factory ChatPhoto.fromMap(Map map) => new ChatPhoto(map["small_file_id"],map["big_file_id"]);

    String get smallFileId => _smallFileId;
    String get bigFileId => _bigFileId;
}


class Chat{
    int _id;
    String _type;
    String _title;
    String _username;
    String _firstName;
    String _lastName;
    bool _AMAA; // all_members_are_admin
    ChatPhoto _photo;
    String _description;
    String _inviteLink;

    int get id => _id;
    String get type => _type;
    String get title => _title;
    String get username => _username;
    String get firstName => _firstName;
    String get lastName => _lastName;
    bool get amaa => _AMAA;
    ChatPhoto get photo => _photo;
    String get description => _description;
    String get inviteLink => _inviteLink;

    Chat(
        this._id,
        this._type,
        [
            this._title,
            this._username,
            this._firstName,
            this._lastName,
            this._AMAA,
            this._photo,
            this._description,
            this._inviteLink
        ]
    );

    factory Chat.fromMap(Map map) => new Chat(
        map["id"],
        map["type"],
        map["title"],
        map["username"],
        map["first_name"],
        map["last_name"],
        map["all_members_are_administrators"],
        map.containsKey("photo")? new ChatPhoto.fromMap(map["photo"]):null,
        map["description"],
        map["invite_link"]
    );
}


class Message{
    int id;
    User from;
    DateTime date;
    Chat chat;
    User forwardFrom;
    Chat forwardFromChat;
    int forwardFromMessageId;
    DateTime forwardDate;
    Message replyToMessage;
    String text;

    Message(
        this.id,
        this.from,
        this.date,
        this.chat,
        [
            this.forwardFrom,
            this.forwardFromChat,
            this.forwardFromMessageId,
            this.forwardDate,
            this.replyToMessage,
            this.text,
        ]
    );

    factory Message.fromMap(Map map) => new Message(
        map["id"],
        new User.fromMap(map["from"]),
        new DateTime.fromMillisecondsSinceEpoch(map["date"]),
        new Chat.fromMap(map["chat"]),
        map.containsKey("forward_from")? new User.fromMap(map["forward_from"]):null,
        map.containsKey("forward_from_chat")? new Chat.fromMap(map["forward_from_chat"]):null,
        map["forward_from_message_id"],
        map.containsKey("forward_date")? new DateTime.fromMillisecondsSinceEpoch(map["forward_date"]):null,
        map.containsKey("reply_to_message")? new Message.fromMap(map["reply_to_message"]):null,
        map["text"]
    );
}


class CallbackQuery{
    String id;
    User from;
    Message message;
    String inlineMessageId;
    String chatInstance;
    String data;
    String gameShortName;

    CallbackQuery(
        this.id,
        this.from,
        [
            this.message,
            this.inlineMessageId,
            this.chatInstance,
            this.data,
            this.gameShortName
        ]
    );

    factory CallbackQuery.fromMap(Map map) => new CallbackQuery(
        map["id"],
        new User.fromMap(map["from"]),
        map.containsKey("message")? new Message.fromMap(map["message"]):null,
        map["inline_message_id"],
        map["chat_instance"],
        map["data"],
        map["game_short_name"]
    );
}


class TelegramBotApplicationInterface{
    String _key;
    int _latestUpdateId;
    bool _stillGettingUpdates; // TODO:  use a locker instead
    Stream<Message> updates;
    StreamController<Message> _updatesCon;
    Stream<CallbackQuery> callbackQuerys;
    StreamController<CallbackQuery> _cbqCon;
    static const String baseAddress = "https://api.telegram.org";

    TelegramBotApplicationInterface(this._key){
        createStreams();
        _latestUpdateId = null;
        _stillGettingUpdates = false;
    }

    String get key => _key;

    String getByMethod(String methodName) => "$baseAddress/bot$_key/$methodName";

    Future<APIBack> callMethod(APIRequest req){
        Completer<APIBack> cpte = new Completer<APIBack>();
        var prog = logger.progress("Request '${req.method}',data: ${req.data}");
        reqs.post(getByMethod(req.method), json: req.data, callback: processContentTypeAsJson)
            ..then((reqs.Response res) async{
                dynamic json = await res.json;
                APIBack back = new APIBack(req, json);
                prog.finish(message: "OK.");
                logger.trace("Method ${req.method}, Data: ${req.data} => API Back: $json");
                cpte.complete(back);
            });
        return cpte.future;
    }

    Future getUpdates(){
        if (_stillGettingUpdates){
            return;
        } else {
            _stillGettingUpdates = true;
        }
        Completer cpte = new Completer();
        Map data = {};
        if (_latestUpdateId != null){
            data["offest"] = _latestUpdateId;
        }
        callMethod(new APIRequest("getUpdates", data))
            .then((APIBack back){
                _stillGettingUpdates = false;
                if (!back.ok){
                    cpte.completeError(new Exception("Method getUpdates fail $back"));
                    return;
                }
                back.data["result"].forEach((Map data){
                    _latestUpdateId = data["update_id"] + 1;
                    if (data["message"] != null){
                        _updatesCon.add(new Message.fromMap(data["message"]));
                    } else if (data["callback_query"] != null){
                        _cbqCon.add(new CallbackQuery.fromMap(data["callback_query"]));
                    }
                });
                logger.trace("Current update id $_latestUpdateId");
        });
        return cpte.future;
    }

    void createStreams(){
        _updatesCon = new StreamController.broadcast();
        updates = _updatesCon.stream;
        _cbqCon = new StreamController.broadcast();
        callbackQuerys = _cbqCon.stream;
    }

    Future<User> getMe(){
        Completer<User> cpte = new Completer();
        callMethod(new APIRequest("getMe"))
            .then((APIBack back) => cpte.complete(new User.fromMap(back.data)));
        return cpte.future;
    }

    Future<Message> sendMessage(int chatId,String text,{String parseMode,
        bool disableWebPagePreview,bool disableNotification,
        int replyToMessageId,KeybroadMarkup replyMarkup}){
            Completer<Message> cpte = new Completer();
            var data = {
                "chat_id": chatId,
                "text": text
            };
            if (parseMode != null) data["parse_mode"] = parseMode;
            if (disableWebPagePreview != null) data["disable_web_page_preview"] = disableWebPagePreview;
            if (disableNotification != null) data["reply_to_message_id"] = replyToMessageId;
            if (replyMarkup != null) data["reply_markup"] = replyMarkup.asMap();
            if (replyToMessageId != null) data["reply_to_message_id"] = replyToMessageId;
            callMethod(new APIRequest("sendMessage", data))
                .then((APIBack back){
                    if (back.ok){
                        cpte.complete(new Message.fromMap(back.data["result"]));
                    } else {
                        cpte.completeError(new Exception("Method sendMessage fail. $data"));
                        logger.trace("[ERROR] Message could not send, API back: $back");
                    }
                });
            return cpte.future;
        }
    
    Future<bool> answerCallbackQuery(String callbackQueryId,
    {String text,bool showAlert,String url,int cacheTime}){
        Completer<bool> cpte = new Completer();
        var data = {
            "callback_query_id": callbackQueryId
        };
        if (text != null) data["text"] = text;
        if (showAlert != null) data["show_alert"] = showAlert;
        if (url != null) data["url"] = url;
        if (cacheTime != null) data["cache_time"] = cacheTime;
        callMethod(new APIRequest("answerCallbackQuery", data))
            .then((APIBack back) => cpte.complete(back.ok));
        return cpte.future;
    }

    Future<bool> editMessageTextById(int chatId,int messageId,String text,{
        String parseMode,
        bool disableWebPagePreview,KeybroadMarkup replyMarkup
    }){
        Completer<bool> cpte = new Completer();
        var data = {
            "chat_id": chatId,
            "message_id": messageId,
            "text": text
        };
        if (parseMode != null) data["parse_mode"] = parseMode;
        if (disableWebPagePreview != null) data["disable_web_page_preview"] = disableWebPagePreview;
        if (replyMarkup != null) data["reply_markup"] = replyMarkup.asMap();
        callMethod(new APIRequest("editMessageText",data))
            .then((APIBack back){
                cpte.complete(back.ok);
            });
        return cpte.future;
    }


    Future<Message> replyMessage(Message replyTo, String text,{String parseMode,
        bool disableWebPagePreview,bool disableNotification,KeybroadMarkup replyMarkup}
    ){
        return sendMessage(
            replyTo.chat.id,
            text,
            parseMode: parseMode,
            disableNotification: disableNotification,
            replyMarkup: replyMarkup,
            replyToMessageId: replyTo.id,
            );
    }
}

