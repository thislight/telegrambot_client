import 'dart:async' show Future,Stream,Timer;
import 'api.dart' show TelegramBotApplicationInterface,Chat,Message,User,CallbackQuery;
import 'router.dart' show Router;
import 'plugin.dart' show Plugin;
import 'keybroad.dart' show KeybroadMarkup;


class TelegramBot{
    TelegramBotApplicationInterface api;
    Map<String,Stream> _streams;
    bool _stopFlag = false;

    TelegramBot(String token){
        api = new TelegramBotApplicationInterface(token);
        _streams = {};
        _putStreams();
    }

    void _putStreams(){
        putEventStream("message", api.updates);
        putEventStream("callbackQuery", api.callbackQuerys);
    }

    void putEventStream<T>(String name,Stream<T> s) => _streams[name] = s;

    Stream<T> getEventStream<T>(String name) => _streams[name];

    void when<T>(String name,Function f) => getEventStream<T>(name).listen(f);

    void addRouter(Router r) => when("message",r.router);

    void use(Plugin p) => p.init(this);

    void _createLongPollingTimer([Duration reptime = const Duration(milliseconds: 200)]){
        new Timer.periodic(reptime, (Timer timer) async{
            if (_stopFlag){
                timer.cancel();
                return;
            }
            await api.getUpdates();
        });
    }

    Future<Message> send(Chat chat,String text,{String parseMode,
        bool disableWebPagePreview,bool disableNotification,
        int replyToMessageId,KeybroadMarkup replyMarkup}){
            return api.sendMessage(
                chat.id,
                text,
                parseMode: parseMode,
                disableNotification: disableNotification,
                disableWebPagePreview: disableWebPagePreview,
                replyMarkup: replyMarkup,
                replyToMessageId: replyToMessageId,
            );
        }
    
    Future<bool> answerCallbackQuery(CallbackQuery query,{String text,bool showAlert,String url,int cacheTime}){
        return api.answerCallbackQuery(
            query.id,
            text: text,
            showAlert: showAlert,
            url: url,
            cacheTime: cacheTime,
        );
    }

    Future<bool> editMessageText(Message message,String text,
    {String parseMode,bool disableWebPagePreview,KeybroadMarkup replyMarkup}){
        return api.editMessageTextById(message.chat.id, message.id, text,parseMode: parseMode,disableWebPagePreview: disableWebPagePreview,replyMarkup: replyMarkup);
    }

    Future<Message> reply(Message replyTo, String text,{String parseMode,
        bool disableWebPagePreview,bool disableNotification,KeybroadMarkup replyMarkup}
    ) => api.replyMessage(
        replyTo,
        text,
        disableNotification: disableNotification,
        disableWebPagePreview: disableWebPagePreview,
        parseMode: parseMode,
        replyMarkup: replyMarkup,
        );

    void start(){
        _createLongPollingTimer();
    }

    void stop() => _stopFlag = true;

    Future<User> get me => api.getMe();
}
