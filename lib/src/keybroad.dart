abstract class KeybroadMarkup{
    Map<String, dynamic> asMap();
}

abstract class HasButtonKeybroadMarkup<T extends KeybroadButton> extends KeybroadMarkup{
    void add(T button);
}

abstract class KeybroadButton{
    Map<String, dynamic> asMap();
}

class InlineKeybroadButton implements KeybroadButton{
    String text;
    String url;
    String callbackData;
    String switchInlineQuery;
    String switchInlineQueryCurrentChat;
    // CallbackGame callbackGame;
    bool pay;

    InlineKeybroadButton(
        this.text,{this.url,this.callbackData,
        this.switchInlineQuery,this.switchInlineQueryCurrentChat,this.pay}
        );
    
    @override
    Map<String, dynamic> asMap(){
        return {
            "text": text,
            "url": url,
            "callback_data": callbackData,
            "switch_inline_query": switchInlineQuery,
            "switch_inline_query_current_chat": switchInlineQueryCurrentChat,
            "callback_game": null,
            "pay": pay
        };
    }
}


class InlineKeybroadMarkup implements HasButtonKeybroadMarkup<InlineKeybroadButton>{
    List<InlineKeybroadButton> buttons;
    
    InlineKeybroadMarkup(this.buttons);

    @override
    void add(InlineKeybroadButton button){
        buttons.add(button);
    }

    @override
    Map<String, dynamic> asMap() => {
        "inline_keybroad": new List.from(buttons.map((var el) => [el])) // put button stand in line up to down
    };
}
