import 'api.dart' show Message;


typedef void MessageHandler(Message message, RouterContext context);


abstract class Router{
    void router(Message message);
}


abstract class MutRouter<T> extends Router{
    void add(T rule, MessageHandler handler);
}


abstract class RouterContext{
    dynamic operator[](String name);
    void operator[]=(String name, dynamic object);
}


class DefaultRouterContext implements RouterContext{
    Map<String, dynamic> _objects;

    DefaultRouterContext(this._objects);

    @override
    dynamic operator[](String name) => _objects[name];

    @override
    void operator[]=(String name, dynamic object) => _objects[name] = object;
}


class RegexpRouter implements MutRouter<String>{
    Map<RegExp, MessageHandler> handlers;

    RegexpRouter(this.handlers);

    factory RegexpRouter.empty() => new RegexpRouter({});

    @override
    void router(Message message){
        if (message.text == null) return;
        handlers.forEach((RegExp exp, MessageHandler handler){
            if (exp.hasMatch(message.text)) handler(message, null);
        });
    }

    @override
    void add(String exp, MessageHandler handler) => handlers[new RegExp(exp)] = handler;
}


class CMDLikeRouter implements MutRouter<String>{
    Map<String, MessageHandler> handlers;

    CMDLikeRouter(this.handlers);

    factory CMDLikeRouter.empty() => new CMDLikeRouter({});

    @override
    void router(Message message){
        if ((message.text == null) || (!message.text.startsWith("/"))) return;
        var clist = message.text.split(" ");
        var c = clist[0].toLowerCase().substring(1);
        handlers.forEach((String cmd, MessageHandler handler){
            if (c == cmd) handler(message, new DefaultRouterContext({
                "params": clist.sublist(1)
            }));
        });
    }

    @override
    void add(String cmd, MessageHandler handler) => handlers[cmd] = handler;
}
